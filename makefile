# *************************************************************
# 'makefile'
# This is a generic 'makefile' for a program
# 
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 12-02-2016--17:37:01
# *************************************************************

# use compiler c++ version 11
GPP = g++ -O3 -Wall -std=c++11
# include utilities folder
UTILS = ../../Utilities
SCANNER = ../../Utilities
SCANLINE = ../../Utilities

M = main.o
C = configuration.o
S = scanner.o
SIM = simulation.o
SL = scanline.o
PCT = onepct.o
VOTE = onevoter.o
R = myrandom.o
U = utils.o


# output of Aprog program
Aprog: $(M) $(C) $(SIM) $(PCT) $(VOTE) $(R) $(S) $(SL) $(U)  
	$(GPP) -o Aprog $(M) $(C) $(SIM) $(PCT) $(VOTE) $(R) $(S) $(SL) $(U) 

# main.cc file
main.o: main.h main.cc
	$(GPP) -c main.cc

# scanner.cc utils file
scanner.o: $(UTILS)/scanner.h $(UTILS)/scanner.cc
	$(GPP) -c $(UTILS)/scanner.cc

# scanline.cc utils file
scanline.o: $(UTILS)/scanline.h $(UTILS)/scanline.cc
	$(GPP) -c $(UTILS)/scanline.cc

# utils.cc utils file
utils.o: $(UTILS)/utils.h $(UTILS)/utils.cc
	$(GPP) -c $(UTILS)/utils.cc

# myrandom.o utils file
myrandom.o: $(UTILS)/myrandom.h $(UTILS)/myrandom.cc
	$(GPP) -c $(UTILS)/myrandom.cc

# *************************************************************
# Other files to compile in this program, "non-default" files
# *************************************************************

# configuration.cc
configuration.o: configuration.h configuration.cc
	$(GPP) -c configuration.cc

# simulation.cc
simulation.o: simulation.h simulation.cc
	$(GPP) -c simulation.cc

# onepct.cc
onepct.o: onepct.h onepct.cc
	$(GPP) -c onepct.cc

# onevoter.cc
onevoter.o: onevoter.h onevoter.cc
	$(GPP) -c onevoter.cc

# *************************************************************
# End 'makefile'
# *************************************************************




