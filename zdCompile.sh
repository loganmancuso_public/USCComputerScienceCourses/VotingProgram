# *************************************************************
# 'zdCompile.sh'
# this program will compile the files in the test directory
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 12-02-2016--17:37:01
# *************************************************************
#!/bin/bash
echo "Descend into 'TestDirectory' directory"
cd TestDirectory/
#
for item in *
do
  echo " "
  echo "COMPILING" $item
  cd $item
  rm *.o
  rm Aprog
  make -f ../../makefile
  cd ..
done
echo "Return from 'TestDirectory' directory"
cd ..
#
echo " "
echo "COMPILING COMPLETE"
echo " "
# *************************************************************
# End 'zdCompile.sh'
# *************************************************************

